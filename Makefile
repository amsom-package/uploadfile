git_add_version:
	AMSOM_UPLOAD_FILE_PACKAGE_VERSION=$$(grep -o '"version": "[^"]*' package.json | grep -o '[^"]*$$') && echo $${AMSOM_UPLOAD_FILE_PACKAGE_VERSION}\
	&& git tag -a v$$AMSOM_UPLOAD_FILE_PACKAGE_VERSION -m "version $${AMSOM_UPLOAD_FILE_PACKAGE_VERSION} from ci-cd" && git push origin v$$AMSOM_UPLOAD_FILE_PACKAGE_VERSION

publish:
	git checkout main && git pull origin main && npm i && npm run lint && ./check-version.sh && make git_add_version && npm publish --access public
