# AMSOM UploadFile

Ce package propose un composant d'envoie de fichier.

## Installation

```bash
npm i @amsom-habitat/upload-file
```

## Développment

Après avoir fait vos dev, veillez à bien tenir à jour le [changelog.md](changelog.md) ainsi que la version du package.json puis faites :
```bash
git add .
git commit -m '<commentaire'
git push origin <branch>
```

## Tests

Les tests sont réalisé de manière automatique sur les branches main et dev mais peuvent être fait localement, notemment pour voir l'evolution du développement via la commande :
```bash
npm run storybook
```

Le valideur devra, si des changements sont observés, aller sur la pipeline pour valider les différences à l'aide de chromatic, sans cela aucun merge-request ne sera possible. Si un merge est effectué, une double verification sera necessaire.

## Déploiement

Après avoir merge les dev sur la branche main, exécutez :
```bash
make publish
```
Cette commande vérifie la version, le changelog et publie le tout

## Utilisation

#### Props

- `name` : Nom de l'input
- `pjName` : Nom de la pj à afficher
- `fileName` : Nom du fichier à stocker
- `subText` : Indication complementaire
- `title` : Titre une fois le fichier chargé
- `required` : Indique si le fichier est requis
- `label` : Label si il est différent de pjName pour l'input
- `modelValue` : Objet contenant les valeurs du fichier
- `gedFile` : Objet contenant le fichier ged le cas echeant
- `readStatus` : Indique l'etat de lecture du fichier
- `readOnly` : Indique si le champ est en lecture seule
- `rejectStatus` : Indique si le fichier a été rejeté
- `clickFileHandler` : Surcharge de la fonction à appeler lors du click sur le fichier

#### Emits

- `delete-file` : Quand le fichier est supprimé
- `file-loading` : Quand le chargement change de status


#### Example complet
```tsx
<tempate>
    <span v-if="fileLoadingCount > 0">Chargement en cours...</span>
    <amsom-upload-file
      v-model="individu.pjCMI"
      required
      name="cmi"
      pj-name="Carte Mobilité Inclusion"
      label="Carte Mobilité Inclusion recto/verso *"
      fileName="TOTO Dupont - Carte mobilité inclusion"
      title="Pièce(s) justificative(s)"
      @delete-file="delete individu.pjCMI"
      @file-loading="updateFileLoading"
    />
</template>

<script>
import {AmsomUploadFile} from '@amsom-habitat/upload-file'

  export default {
    name: 'TestPage',
    components: {
      AmsomUploadFile,
    },
    data() {
        return {
            individu: {
              id: 1,
              nom: 'Dupont',
              prenom: 'Toto'
            },
            fileLoadingCount: 0
        }
    },
    methods: {
      updateFileLoading(isLoading) {
          if (isLoading) this.fileLoadingCount++;
          else this.fileLoadingCount--;
      }
    }
}
</script>
```
