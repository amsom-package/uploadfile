import AmsomUploadFile from '../AmsomUploadFile.vue';

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories
export default {
  title: 'AmsomUploadFile',
  component: AmsomUploadFile,
  tags: ['autodocs'],
  argTypes: {
    //backgroundColor: {
    //  control: 'color',
    //},
    //onClick: {},
    //size: {
    //  control: {
    //    type: 'select',
    //  },
    //  options: ['small', 'medium', 'large'],
    //},
  },
};

// More on writing stories with args: https://storybook.js.org/docs/writing-stories/args
export const Default = {
  args: {
    name: "cmi",
    pjName: "Carte mobilité inclusion",
    subText: "Copie recto verso de la carte mobilité inclusion",
    fileName: "cmi-test",
    title: "Pièces justificatives",
    required: true,
    onDeleteFile: () => {
      console.log('deleted')
      alert('deleted')
    },
    onFileLoading: () => {
      console.log('loading')
      alert('loading')
    }
  },
};

export const CustomOpen = {
  args: {
    name: "cmi",
    pjName: "Carte mobilité inclusion",
    subText: "Copie recto verso de la carte mobilité inclusion",
    fileName: "cmi-test",
    title: "Pièces justificatives",
    required: true,
    modelValue: {},
    onDeleteFile: () => {
      console.log('deleted')
      alert('deleted')
    },
    onFileLoading: () => {
      console.log('loading')
      alert('loading')
    },
    clickFileHandler: (file) => {
      console.log(file)
      alert('custom open file')
    }
  },
};
